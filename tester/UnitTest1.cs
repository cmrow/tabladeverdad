using Microsoft.VisualStudio.TestTools.UnitTesting;
using condicionales;

namespace tester
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Verdadero_Debe_Retornar_Falso_Al_Mensaje_Not()
        {
            Verdadero verdadero = new Verdadero();
            Assert.AreEqual(Booleano.falso, verdadero.Not());
        }

        [TestMethod]
        public void Falso_Debe_Retornar_Verdadero_Al_Mensaje_Not()
        {
            Falso falso = new Falso();
            Assert.AreEqual(Booleano.verdadero, falso.Not());
        }

        [TestMethod]
        public void Verdadero_Debe_Retornar_Verdadero_Al_Mensaje_And_Contra_Verdadero()
        {
            Verdadero verdadero = new Verdadero();
            Assert.AreEqual(Booleano.verdadero, verdadero.And(Booleano.verdadero));
        }

        [TestMethod]
        public void Verdadero_Debe_Retornar_Falso_Al_Mensaje_And_Contra_Falso()
        {
            Verdadero verdadero = new Verdadero();
            Assert.AreEqual(Booleano.falso, verdadero.And(Booleano.falso));
        }

        [TestMethod]
        public void Falso_Debe_Retornar_Falso_Al_Mensaje_And_Contra_Verdadero()
        {
            Falso falso = new Falso();
            Assert.AreEqual(Booleano.falso, falso.And(Booleano.verdadero));
        }

        [TestMethod]
        public void Falso_Debe_Retornar_Falso_Al_Mensaje_And_Contra_Falso()
        {
            Falso falso = new Falso();
            Assert.AreEqual(Booleano.falso, falso.And(Booleano.falso));
        }

        [TestMethod]
        public void Verdadero_Debe_Retornar_Verdadero_Al_Mensaje_Or_Contra_Verdadero()
        {
            Verdadero verdadero = new Verdadero();
            Assert.AreEqual(Booleano.verdadero, verdadero.And(Booleano.verdadero));
        }

        [TestMethod]
        public void Verdadero_Debe_Retornar_Verdadero_Al_Mensaje_Or_Contra_Falso()
        {
            Verdadero verdadero = new Verdadero();
            Assert.AreEqual(Booleano.verdadero, verdadero.Or(Booleano.falso));
        }

        [TestMethod]
        public void Falso_Debe_Retornar_Verdadero_Al_Mensaje_Or_Contra_Verdadero()
        {
            Falso falso = new Falso();
            Assert.AreEqual(Booleano.verdadero, falso.Or(Booleano.verdadero));
        }

        [TestMethod]
        public void Falso_Debe_Retornar_Falso_Al_Mensaje_Or_Contra_Falso()
        {
            Falso falso = new Falso();
            Assert.AreEqual(Booleano.falso, falso.Or(Booleano.falso));
        }


    }
}
