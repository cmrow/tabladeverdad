using System.Collections.Generic;
using System.Linq;
using condicionales;

namespace condicionales
{
    public class Verdadero : Booleano
    {
        public override Booleano Not()
        {
            return Booleano.falso;
        }

        public override Booleano And(Booleano aBooleano)
        {
            return aBooleano;
        }

        public override Booleano Or(Booleano aBooleano)
        {
            return Booleano.verdadero;
        }
    }

}