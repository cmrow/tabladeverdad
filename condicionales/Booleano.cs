using System.Collections.Generic;
using System.Linq;

namespace condicionales
{
    public abstract class Booleano
    {
        public static Booleano verdadero = new Verdadero();
        public static Booleano falso = new Falso();

        public abstract Booleano Not();
        public abstract Booleano And(Booleano aBooleano);
         public abstract Booleano Or(Booleano aBooleano);

    }
}