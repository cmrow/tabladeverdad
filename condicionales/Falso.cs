using System.Collections.Generic;
using System.Linq;
using condicionales;

namespace condicionales
{
    public class Falso : Booleano
    {
        public override Booleano Not()
        {
            return Booleano.verdadero;
        }

        public override Booleano And(Booleano aBooleano)
        {
            return Booleano.falso;
        }

         public override Booleano Or(Booleano aBooleano)
         {
             return aBooleano;
         }
         

    }

}